package br.com.lillianlutzner.springcourse.resources;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.lillianlutzner.springcourse.entities.User;

@RestController
@RequestMapping(value="/users")
public class UserResource {
	
	@GetMapping
	public ResponseEntity<User> findAll() {
		User u = new User(1L, "Lillian", "lillian@gmail.com", "99999999", "jj1i1i1ji1");
		return ResponseEntity.ok().body(u);
	}

}
